from django.db import models

class Year(models.Model):
    Year_Choices=(("2019","2019"),("2018","2018"),("2017","2017"),("2016","2016"))
    year = models.CharField(choices=Year_Choices,default="2019",null="True",max_length=4)

    def __str__(self):
        return '%s' % self.year

class Friends(models.Model):
    name = models.CharField(max_length=100)
    hobby = models.CharField(max_length=100)
    food = models.CharField(max_length=100)
    drink = models.CharField(max_length=100)
    the_year=models.ForeignKey(Year,on_delete=models.CASCADE)
    
    def __str__(self):
        return '%s' % self.name
