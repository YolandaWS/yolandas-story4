from django.forms import ModelForm
from .models import Friends,Year

class FriendsForm(ModelForm):
    class Meta:
        model=Friends
        fields='__all__'