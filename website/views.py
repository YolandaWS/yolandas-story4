from django.shortcuts import render
from MyApp.forms import FriendsForm
from MyApp.models import Friends
from django.http import HttpResponseRedirect

#methodview
def index(request):
    return render(request,'Profile.html')

def friends(request):
    friends =Friends.objects.all()
    if request.method=="POST":
        form=FriendsForm(request.POST)
        if form.is_valid():
            form.save()
            return render (request,'Thanks.html',{'form':form})

    else:
        form=FriendsForm()
    
    return render(request,'Friends.html',{'form':form, 'friends':friends})

def thanks(request):
    return render(request,'Thanks.html')